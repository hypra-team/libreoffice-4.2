From 0aaa81b2760dde7e84c85b8caacc20552910ffe9 Mon Sep 17 00:00:00 2001
From: Dennis Francis <dennis.francis@collabora.co.uk>
Date: Tue, 25 Jul 2017 17:11:47 +0530
Subject: tdf#71409: Pre-create sum/equal and ok/cancel buttons...

in the sc formula bar at the start instead of adding/
removing them to/from vcl ToolBox when required.
To maintain the functionality, do hide/show and
enable/disable the button pairs (sum/equal, ok/cancel)
during mode switch. This solves the excess a11y events
problem of tdf#71409 because this patch get rid of
the usages of vcl's Toolbox::Remove() and its accessibility
module dependencies which seems to be the root cause.

Change-Id: Ib953fb64d25a4b83018eca6a88223c7236c2f72e
Reviewed-on: https://gerrit.libreoffice.org/40479
Tested-by: Jenkins <ci@libreoffice.org>
Reviewed-by: Dennis Francis <dennis.francis@collabora.co.uk>
(cherry picked from commit 561cae8e81913940e4af86901ec46a484669c597)
Reviewed-on: https://gerrit.libreoffice.org/40827
Reviewed-by: Michael Stahl <mstahl@redhat.com>
Tested-by: Michael Stahl <mstahl@redhat.com>
---
 sc/source/ui/app/inputwin.cxx | 55 ++++++++++++++++++++++++++++---------------
 1 file changed, 36 insertions(+), 19 deletions(-)

diff --git a/sc/source/ui/app/inputwin.cxx b/sc/source/ui/app/inputwin.cxx
index 70957b3..dcc5ce7 100644
--- a/sc/source/ui/app/inputwin.cxx
+++ b/sc/source/ui/app/inputwin.cxx
@@ -190,8 +190,10 @@ ScInputWindow::ScInputWindow( vcl::Window* pParent, SfxBindings* pBind ) :
     InsertItem      ( SID_INPUT_FUNCTION, IMAGE( SID_INPUT_FUNCTION ), 0, 2 );
     InsertItem      ( SID_INPUT_SUM,      IMAGE( SID_INPUT_SUM ), 0,      3 );
     InsertItem      ( SID_INPUT_EQUAL,    IMAGE( SID_INPUT_EQUAL ), 0,    4 );
-    InsertSeparator (                                                     5 );
-    InsertWindow    ( 7, &aTextWindow, 0,                                 6 );
+    InsertItem      ( SID_INPUT_CANCEL,   IMAGE( SID_INPUT_CANCEL ), 0,   5 );
+    InsertItem      ( SID_INPUT_OK,       IMAGE( SID_INPUT_OK ), 0,       6 );
+    InsertSeparator (                                                     7 );
+    InsertWindow    ( 7, &aTextWindow, 0,                                 8 );
 
     aWndPos    .SetQuickHelpText( ScResId( SCSTR_QHELP_POSWND ) );
     aWndPos    .SetHelpId       ( HID_INSWIN_POS );
@@ -208,6 +210,18 @@ ScInputWindow::ScInputWindow( vcl::Window* pParent, SfxBindings* pBind ) :
     SetItemText ( SID_INPUT_EQUAL, aTextEqual );
     SetHelpId   ( SID_INPUT_EQUAL, HID_INSWIN_FUNC );
 
+    SetItemText ( SID_INPUT_CANCEL, aTextCancel );
+    SetHelpId   ( SID_INPUT_CANCEL, HID_INSWIN_CANCEL );
+
+    SetItemText ( SID_INPUT_OK, aTextOk );
+    SetHelpId   ( SID_INPUT_OK, HID_INSWIN_OK );
+
+    EnableItem( SID_INPUT_CANCEL, false );
+    EnableItem( SID_INPUT_OK, false );
+
+    HideItem( SID_INPUT_CANCEL );
+    HideItem( SID_INPUT_OK );
+
     SetHelpId( HID_SC_INPUTWIN );   // fuer die ganze Eingabezeile
 
     aWndPos     .Show();
@@ -504,14 +518,16 @@ void ScInputWindow::SetOkCancelMode()
     SfxImageManager* pImgMgr = SfxImageManager::GetImageManager( pScMod );
     if (!bIsOkCancelMode)
     {
-        RemoveItem( 3 ); // SID_INPUT_SUM und SID_INPUT_EQUAL entfernen
-        RemoveItem( 3 );
-        InsertItem( SID_INPUT_CANCEL, IMAGE( SID_INPUT_CANCEL ), 0, 3 );
-        InsertItem( SID_INPUT_OK,     IMAGE( SID_INPUT_OK ),     0, 4 );
-        SetItemText ( SID_INPUT_CANCEL, aTextCancel );
-        SetHelpId   ( SID_INPUT_CANCEL, HID_INSWIN_CANCEL );
-        SetItemText ( SID_INPUT_OK,     aTextOk );
-        SetHelpId   ( SID_INPUT_OK,     HID_INSWIN_OK );
+        EnableItem  ( SID_INPUT_SUM,   false );
+        EnableItem  ( SID_INPUT_EQUAL, false );
+        HideItem    ( SID_INPUT_SUM );
+        HideItem    ( SID_INPUT_EQUAL );
+
+        ShowItem    ( SID_INPUT_CANCEL, true );
+        ShowItem    ( SID_INPUT_OK,     true );
+        EnableItem  ( SID_INPUT_CANCEL, true );
+        EnableItem  ( SID_INPUT_OK,     true );
+
         bIsOkCancelMode = sal_True;
     }
 }
@@ -524,15 +540,16 @@ void ScInputWindow::SetSumAssignMode()
     SfxImageManager* pImgMgr = SfxImageManager::GetImageManager( pScMod );
     if (bIsOkCancelMode)
     {
-        // SID_INPUT_CANCEL, und SID_INPUT_OK entfernen
-        RemoveItem( 3 );
-        RemoveItem( 3 );
-        InsertItem( SID_INPUT_SUM,   IMAGE( SID_INPUT_SUM ),     0, 3 );
-        InsertItem( SID_INPUT_EQUAL, IMAGE( SID_INPUT_EQUAL ),   0, 4 );
-        SetItemText ( SID_INPUT_SUM,   aTextSum );
-        SetHelpId   ( SID_INPUT_SUM,   HID_INSWIN_SUMME );
-        SetItemText ( SID_INPUT_EQUAL, aTextEqual );
-        SetHelpId   ( SID_INPUT_EQUAL, HID_INSWIN_FUNC );
+        EnableItem  ( SID_INPUT_CANCEL, false );
+        EnableItem  ( SID_INPUT_OK,     false );
+        HideItem    ( SID_INPUT_CANCEL );
+        HideItem    ( SID_INPUT_OK );
+
+        ShowItem    ( SID_INPUT_SUM,    true );
+        ShowItem    ( SID_INPUT_EQUAL,  true );
+        EnableItem  ( SID_INPUT_SUM,    true );
+        EnableItem  ( SID_INPUT_EQUAL,  true );
+
         bIsOkCancelMode = false;
 
         SetFormulaMode(false);      // kein editieren -> keine Formel
-- 
cgit v1.1

