#!/bin/sh
set -e

# Upstream version, and target version for the generated binaries:
upstream=4.2.6.3
target=4.2.6.6

# Not using a proxy is a huge penalty, encourage it:
if [ -z "$http_proxy" ]; then
  echo "W: http_proxy isn't set; using a proxy is usually a good idea when building LO"
  echo "W: sleeping 15 seconds to make sure this message is seen"
  sleep 15
fi

# Build dependencies:
sudo apt-get build-dep -y libreoffice
sudo apt-get install -y libkrb5-dev libgconf2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev graphviz git wget

# This could be made conditional based on lsb_release's output:
sudo apt-get install -y gcc-4.8 g++-4.8
# List compiled based on the available *-4.8 command after the
# installation of the gcc and g++ packages in version 4.8:
echo "Warning: pointing gcc/g++ to their 4.8 versions"
all='/usr/bin/g++ /usr/bin/gcc /usr/bin/gcc-ar /usr/bin/gcc-nm /usr/bin/gcc-ranlib /usr/bin/gcov'
for i in $all; do sudo ln -sf $i-4.8 $i; done

# Directory with wrapper and patches (libreoffice-4.2.git):
DIR=$(dirname $(readlink -f "$0"))

# Check tarball's existence:
tarball="$DIR/libreoffice-$upstream.tar.xz"
if [ ! -e "$tarball" ]; then
  wget "http://download.documentfoundation.org/libreoffice/src/4.2.6/$(basename "$tarball")" -O "$tarball"
fi
if [ ! -f "$tarball" -a ! -L "$tarball" ]; then
  echo "E: missing upstream tarball => $tarball"
  exit 1
fi

# Check version.diff's existence for specified versions:
version_template="$DIR/version-XXX-YYY.diff"
version_patch="$DIR/version-$upstream-$target.diff"
if [ ! -f "$version_patch" ]; then
  echo "I: missing version.diff for version $upstream/$target, creating"
  sed "s/XXX/$upstream/g;s/YYY/$target/g" "$version_template" > "$version_patch"
fi

# Extract tarball, change directory, and apply patches:
tar xf $tarball
cd libreoffice-$upstream/

list_patches() {
  cat <<EOF
$DIR/build.diff
$DIR/lo-69a80316f7da33e90e1006624466f52af524f1dc.diff
$DIR/source-field.diff
$version_patch
$DIR/backport-5e6cd9b7a15e7e6a994e8f3e4fafed2928dbf42d.diff
$DIR/0001-tdf-105415-Use-the-system-caret-width-on-GTK_LO4.2.patch
$DIR/lo-characters.patch
$DIR/0001-fdo-71558-Notify-mispelled-word-to-accessibility.patch
$DIR/0bc023d55afcf373a3b6644545ce4bae1bb5ca47.patch
$DIR/e4e208fa2b0930be5a7cbbe2fab2ff2fe2c4a1ff.patch
$DIR/9ba24ac54100a5af6ecdde479ebeb5c916e6fad2.patch
$DIR/ae923f941f70ebe99cc785076f3357015dd69003.patch
$DIR/0aaa81b2760dde7e84c85b8caacc20552910ffe9.patch
EOF
}

do_patch() {
  list_patches | while read patch; do
    echo "applying patch $patch..."
    patch -p1 < "$patch" || return $?
  done
}

do_unpatch() {
  list_patches | tac | while read patch; do
    echo "unapplying patch $patch..."
    patch -p1 -R < "$patch" || return $?
  done
}

# apply patches
do_patch

# This ought to be sufficient but isn't:
export CC=gcc-4.8
export CXX=g++-4.8

# Last fix-up, configure, and make:
chmod a+x bin/unpack-sources
./configure --enable-release-build --with-package-format=deb --with-lang=ALL --disable-dependency-tracking --with-vendor=Hypra --enable-epm --without-java --with-help --with-myspell-dicts 2>&1 | tee configure.log
make 2>&1 | tee make.log

# un-apply the patches so applying them again will work
do_unpatch
