#!/bin/sh
set -e

export LC_ALL=C

# Sanity checks:
home=$(dirname $(readlink -f "$0"))
reference="$home/manifest.ref"
for path in x64 x32 "$reference"; do
  if [ ! -e "$path" ]; then
    echo "E: required path $path is missing"
    exit 1
  fi
done

# .deb extraction from the build tree
for arch in x64 x32; do
  mkdir -p $arch.deb
  cd $arch
  for deb in $(find -L -name '*.deb'); do
    cp -v --parents $deb ../$arch.deb
  done
  cd -
done

# Extract list of arch-specific binaries:
(cd x32.deb; find -type f|grep -F _i386.deb|LC_ALL=C sort > ../1)
(cd x64.deb; find -type f|grep -F _amd64.deb|sed 's,x86-64,x86,;s,_amd64,_i386,'|LC_ALL=C sort > ../2)

# Generate list of duplicates:
for i in $(cat 1); do basename $i; done|sort|uniq -c|grep -v '^  *1 ' > 1.dupes
for i in $(cat 2); do basename $i; done|sort|uniq -c|grep -v '^  *1 ' > 2.dupes

# Check everything matches:
cmp 1.dupes 2.dupes || { echo "dupes list not equal"; exit 1; }

# Clean extra packages:
for i in x32.deb x64.deb; do
  cd $i
  # Use libreoffice4.2 deb as a reference. Remove dirname, package name,
  # keep version, remove architecture and extension, then strip the debian
  # revision:
  version=$(find libreoffice-4.2.6.3/workdir/installation/LibreOffice/deb/install -name 'libreoffice4.2_*_*.deb' \
    | sed -e 's,.*/libreoffice4.2_,,' -e 's,_.*,,' -e 's,-.*,,')
  echo "found version: $version"
  echo
  echo "removing sdk:"
  find -name 'libobasis4.2-sdk_*_*.deb' -print -delete
  echo "removing en dicts:"
  find libreoffice-4.2.6.3/workdir/installation/LibreOffice_languagepack/deb/install/LibreOffice_${version}_Linux_*_deb_langpack_en-* -name libreoffice4.2-dict-en_'*' -print -delete
  rm -fv libreoffice-4.2.6.3/workdir/installation/LibreOffice_languagepack/deb/install/LibreOffice_${version}_Linux_*_deb_langpack_en-US/DEBS/*
  echo "removing helppack packages, duplicated as langpack packages:"
  find libreoffice-4.2.6.3/workdir/installation/LibreOffice_helppack/deb/install/LibreOffice_${version}_Linux_*_deb_helppack_*/DEBS/ -name '*.deb' -print -delete

  for lang in ca es fr nn:no sr; do
    echo "removing $lang dicts:"
    d=$(echo "$lang"|sed 's/:.*//')
    f=$(echo "$lang"|sed 's/.*://')
    rm -fv libreoffice-4.2.6.3/workdir/installation/LibreOffice_languagepack/deb/install/LibreOffice_${version}_Linux_*_deb_langpack_$d/DEBS/libreoffice4.2-dict-${f}*
  done

  echo "removing extra libreoffice4.2-debian-menus copy:"
  rm -fv libreoffice-4.2.6.3/workdir/installation/LibreOffice/deb/install/LibreOffice_*_Linux_*_deb/DEBS/libreoffice4.2-debian-menus_*_all.deb

  if [ "$i" = "x32.deb" ]; then
    echo "removing arch:all *-debian-menus packages (x32 case):"
    for package in libreoffice4.2-debian-menus libreofficedev4.2-debian-menus oxygenoffice4.2-debian-menus; do
      rm -fv libreoffice-4.2.6.3/workdir/CustomTarget/sysui/deb/${package}_*_all.deb
    done
  fi

  cd -
done

# Build a manifest
find x32.deb x64.deb -type f > new-manifest
revision=$(find x64.deb/libreoffice-4.2.6.3/workdir/installation/LibreOffice/deb/install -name 'libreoffice4.2_*_*.deb' \
  | sed -e 's,.*/libreoffice4.2_,,' -e 's,_.*,,')
version=$(echo "$revision" | sed 's,-.*,,')
sed -e "s,_${revision}_,_REVISION_,g" -e "s,_${version}_,_VERSION_,g" new-manifest | sort > new-manifest.ref

# Check manifest is expected:
if ! cmp "$reference" new-manifest.ref; then
  echo "E: manifest (new-manifest.ref) doesn't match reference ($reference)"
  diff -u "$reference" new-manifest.ref
  exit 1
fi

echo "I: all looks good"
echo
echo "Next step: find *.deb -type f | LC_ALL=C sort | xargs reprepro -v -S editors -C main -P optional includedeb \$SUITE"
